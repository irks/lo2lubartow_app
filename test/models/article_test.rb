require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  
  def setup
  	@article = articles(:one)
  end

  test "should be valid" do 
  	assert @article.valid?
  end

  test "content should be present" do 
  	@article.body = "  "
  	assert_not @article.valid?
  end

  test "title should be at most 150 characters" do 
  	@article.title = "a" * 151
  	assert_not @article.valid?
  end
end
