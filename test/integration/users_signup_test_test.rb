require 'test_helper'

class UsersSignupTestTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "invalid signup information" do 
  	get new_user_path
  	assert_no_difference 'User.count' do 
  		post users_path, user: { name: "", email: "user@invalid", 
  								password: "abc", password_confirmation: "cba"}
  	end
  	assert_template 'users/new'
  	assert_select 'div#error_explanation'
  	assert_select 'div.field_with_errors'
  end
end
