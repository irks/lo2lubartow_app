require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase

	def setup
		@base_title = "II Liceum Ogólnokształcące w Lubartowie"
	end
  
 test "should get contact" do
 	get :contact
 	assert_response :success
 	assert_select "title", "Kontakt | #{@base_title}"
 end

 test "should get timetable" do 
 	get :timetable
 	assert_response :success
 	assert_select "title", "Plan lekcji | #{@base_title}"
 end
  	
 
end
