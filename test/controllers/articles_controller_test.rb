require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  
  def setup
  	@article = articles(:one)
  end

  test "should redirect create when not logged in" do 
  	assert_no_difference 'Article.count' do
  		post :create, article: { body: "ABC" }
  	end
  	assert_redirected_to login_url
  end
end
