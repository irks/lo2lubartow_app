class Article < ActiveRecord::Base
	validates :title, presence: true, length: { maximum: 150 }
	validates :body, presence: true
end
