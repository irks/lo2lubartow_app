class Post < ActiveRecord::Base
  belongs_to :user
  validates :body, presence: true, length: { maximum: 200 }

  def timestamp
  created_at.strftime('%-d %B %Y, %H:%M:%S')
end
end
