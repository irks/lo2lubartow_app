module ApplicationHelper
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "II Liceum Ogólnokształcące w Lubartowie"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def if_admin(user)
  	if( !user.nil? && user.admin? )
  		yield
  	end
  end
end