class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user

    mail to: user.email, subject: "Aktywacja konta"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Resetowanie hasła"
  end
end
