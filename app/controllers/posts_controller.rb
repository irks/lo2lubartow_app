class PostsController < ApplicationController
	before_action :logged_in_user, only: [:create]
	before_filter :disable_sidebar, only: [:new]

	def new
		@disable_sidebar = true
		@post = Post.new
		@posts = Post.paginate(page: params[:page], per_page: 10).order('created_at DESC' )
		respond_to do |format|
			format.html
			format.js
		end
		
	end

	def create
		if current_user
			@post = current_user.posts.build(post_params)
			if @post.save
				flash[:success] = 'Twój wpis został umieszczony.'
			else
				flash[:error] = 'Twój wpis nie został dodany.'
			end
		end
		redirect_to new_post_path
	end

	private

	def post_params
		params.require(:post).permit(:body)
	end

end
