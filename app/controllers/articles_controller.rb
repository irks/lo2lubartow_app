class ArticlesController < ApplicationController
	before_action :logged_in_user, only: [:new, :create, :edit, :update]
	before_action :admin_user, only: [:new, :create, :edit, :update]
	
	
	def index
		@articles = Article.paginate(page: params[:page], per_page: 10).order('created_at DESC')
		@user = current_user
	end
	def new
			@article = Article.new
	end
	
	def create
		@article = Article.new(article_params)
			if @article.save
				flash[:success] = "Wpis dodany"
				redirect_to articles_path
			else
				flash[:danger] = "Wpis niedodany"
			end
	end

	def show
		@article = Article.find(params[:id])
		@user = current_user
	end

	def edit
			@article = Article.find(params[:id])
			
	end

	def update
		@article = Article.find(params[:id])
		if @article.update(params[:article].permit(:title, :body))
			flash[:success] = "Wpis zaktualizowany"
			redirect_to articles_path
		else
			flash[:danger] = "Wpis nie został zaktualizowany"
			render 'edit'
		end
	end

	private
		
		def article_params
			params.require(:article).permit(:title, :body)
		end

		def admin_user
			redirect_to(root_url) && flash[:danger] = "Nie jesteś adminem" unless current_user.admin?
		end


end
