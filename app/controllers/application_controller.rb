class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  def disable_sidebar
    @disable_sidebar = true
  end
	private
  		
  		# Confirms a logged-in user.
  		def logged_in_user
  			unless logged_in?
  				store_location
  				flash[:danger] = "Zaloguj się!"
  				redirect_to login_url
  			end
  		end
end
