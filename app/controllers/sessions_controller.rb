class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(name: params[:session][:name])
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message  = "Konto nie zostało aktywowane. "
        message += "Poszukaj na koncie poczty elektronicznej email'u z linkiem aktywacyjnym."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Błędny email lub hasło' 
      render 'new'
    end
  end
  def destroy
  	log_out if logged_in?
  	redirect_to root_url
  end

end
