# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!(name:  "example5",
             email: "example5@example.com",
             password:              "example",
             password_confirmation: "example",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now
             )
User.create!(name:  "example",
             email: "example@example.com",
             password:              "example",
             password_confirmation: "example",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now
             )
Article.create!(title: "Zakończenie roku szkolnego",
			body: "Na uroczystości zakończenia roku szkolnego w naszej szkole gościli m. in. Naczelnik Wydziału Oświaty, Kultury i Sportu Ewa Sędzimierz oraz odchodzący na emeryturę ksiądz kanonik Józef Huzar. Młodzież serdecznie żegnała proboszcza parafii Matki Bożej Nieustającej Pomocy i dziękowała za lata współpracy.
Uczniowie po ciężkiej pracy w ciągu dziesięciu miesięcy podsumowali swoje osiągnięcia. Sukcesów w II LO było bardzo wiele, młodzież rozwijała swoje zainteresowania, biorąc udział w konkursach przedmiotowych, sportowych i artystycznych, aktywnie działała w Klubie Wolontariusza, Szkolnym Klubie Krajoznawczo – Turystycznym „Azymut”, Klubie Europejskim, w Samorządzie Uczniowskim, który przez dwa lata przewodził Sejmikowi Dzieci i Młodzieży Miasta Lubartowa, przygotowywała uroczystości szkolne i miejskie, m. in. wieczorki poetyckie, obchody Narodowego Święta Niepodległości, rocznicy Zbrodni Katyńskiej, 20-lecie szkoły czy koncert rockowy. Dyrektor Halina Zdunek oraz Rada Rodziców pod przewodnictwem Dariusza Lipskiego starali się licealistom wynagrodzić chociaż symbolicznie wysiłek i zaangażowanie, najlepsi i najaktywniejsi uczniowie odbierali zasłużone nagrody, książkowe i pieniężne. Część artystyczna zamknęła uroczystość, po której młodzież z klas pierwszych i drugich spotkała się z wychowawcami. 
Kolejny rok szkolny II LO może zaliczyć do udanych. Wakacje to czas na relaks, młodzież z naszego liceum spędza ten czas, wciąż poszerzając swoje umiejętności, nawiązując nowe znajomości, uczestnicząc w międzynarodowych wymianach i wyjazdach. Od 20 do 30 czerwca grupa uczniów przebywała w Rumunii, kolejna grupa szykuje się na wyprawę do Gruzji, inni licealiści będą spotykali się z rówieśnikami z Niemiec, Irlandii i Izraela.
Całej społeczności II LO życzymy udanych, w pełni zasłużonych wakacji!",
			created_at: Time.zone.now,
			)

99.times do |n|
      title = Faker::Lorem.sentence(word_count = 5, random_words_to_add = 1)
      body = Faker::Lorem.sentence(word_count = 50, random_words_to_add = 19)
      created_at = Time.zone.now
      Article.create!(title: title,
            body: body,
            created_at: created_at
            )
end